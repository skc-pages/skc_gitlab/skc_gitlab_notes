.PHONY: test build_base_adoc deploy_public clean_html


BASE_DIR = $(dir $(shell pwd))
DIR = $(notdir $(shell pwd))
ROOT_DIR = ${BASE_DIR}${DIR}
ADOC_DIR = ${ROOT_DIR}/adoc
MOD_DIR = ${ADOC_DIR}/modules
PUBLIC_DIR = ${ROOT_DIR}/public


test:
	echo 'this is a test';
	echo ${BASE_DIR}
	echo ${DIR}
	echo ${ROOT_DIR}
	echo ${ADOC_DIR}
	echo ${MOD_DIR}
	echo ${PUBLIC_DIR}

local_build: local_build_base_adoc local_build_mod_adco

local_build_base_adoc:
	cd ${ADOC_DIR}; \
	pwd ; \
	docker run  -v ${ADOC_DIR}:/documents/ asciidoctor/docker-asciidoctor asciidoctor *.adoc; \
	sudo chown sconklin:sconklin ${ADOC_DIR}/*.html;

local_build_mod_adco:
	cd ${MOD_DIR}; \
	pwd; \
	docker run  -v ${MOD_DIR}:/documents/ asciidoctor/docker-asciidoctor asciidoctor *.adoc; \
	sudo chown sconklin:sconklin ${MOD_DIR}/*.html;

local_clean_html:
	 find . -name *.html | xargs rm;

local_deploy_public:
	cd ${ADOC_DIR}; \
	pwd ; \
	cp -R . ../public; \
	find ${PUBLIC_DIR} -name '*.adoc' -type f -delete; 

#build_and_deploy: build_base_adoc build_mod_adco deploy_public
build_and_deploy: build_base_adoc build_mod_adco deploy_public

build_base_adoc:
	cd ${ADOC_DIR}; \
	pwd ; \
	asciidoctor *.adoc;
	
build_mod_adco:
	cd ${MOD_DIR}; \
	pwd; \
	asciidoctor *.adoc;

deploy_public:
	cd ${ADOC_DIR}; \
	pwd ; \
	cp -R . ../public; \
	find ${PUBLIC_DIR} -name '*.adoc' -type f -delete; 